# SPDX-License-Identifier: GPL-3.0-or-later


def is_python_file(path):
    if path.suffix == ".py":
        return True
    with path.open("rb") as f:
        first_line_bytes = f.readline(1024)
    if not first_line_bytes.startswith(b"#!"):
        return False
    try:
        first_line = first_line_bytes.decode()
    except UnicodeDecodeError:
        return False
    return first_line.endswith("python\n") or first_line.endswith("python3\n")


def iterate_over_files_recursively(path):
    if path.name in [".git", ".venv"]:
        pass
    elif path.is_file():
        yield path
    elif path.is_dir():
        for x in sorted(path.iterdir()):
            yield from iterate_over_files_recursively(x)
