# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path

import scapy.layers.inet
import scapy.utils


def test_echo_reply_packet_captured():
    packets = scapy.utils.rdpcap("link0.pcap")

    assert packets

    icmp_packets = [
        packet for packet in packets if scapy.layers.inet.ICMP in packet
    ]

    assert len(icmp_packets) == 2
    last_icmp_packet = icmp_packets[-1]
    assert (
        scapy.layers.inet.icmptypes[
            last_icmp_packet[scapy.layers.inet.ICMP].type
        ]
        == "echo-reply"
    )
    assert last_icmp_packet[scapy.layers.inet.IP].src == "192.168.0.1"
    assert last_icmp_packet[scapy.layers.inet.IP].dst == "192.168.0.2"


def test_ping_output_logged():
    log_text = Path("debug.log").read_text(encoding="ascii")
    assert "1 packets transmitted, 1 packets received" in log_text


def test_console_output_logged():
    log_text = Path("debug.log").read_text(encoding="ascii")
    assert "Welcome to Alpine!" in log_text


def test_console_output_formatted():
    log_text = Path("debug.log").read_text(encoding="ascii")
    assert r"Welcome to Alpine!\r\n" in log_text


def test_console_output_bytes_logged():
    log_bytes = Path("console.log").read_bytes()
    assert b"Welcome to Alpine!\r\n" in log_bytes
