# SPDX-License-Identifier: GPL-3.0-or-later

import os
import time
from pathlib import Path

import docker
import pytest

import alpy.node
import common


class TestNodeContainer:
    @staticmethod
    def test_when_initialized_then_calling_close_is_ok(docker_client):
        node_container = alpy.node.NodeContainer(
            docker_client=docker_client,
            name="blue",
            timeout=5,
            image=common.IMAGE_BUSYBOX,
        )
        node_container.close()

    @staticmethod
    def test_when_run_method_is_called_then_container_is_running(docker_client):
        node_container = alpy.node.NodeContainer(
            docker_client=docker_client,
            name="blue",
            timeout=5,
            image=common.IMAGE_BUSYBOX,
        )
        node_container.run()
        assert docker_client.containers.get("blue").status == "running"
        node_container.close()

    @staticmethod
    def test_when_close_method_is_called_then_container_is_removed(
        docker_client,
    ):
        node_container = alpy.node.NodeContainer(
            docker_client=docker_client,
            name="blue",
            timeout=5,
            image=common.IMAGE_BUSYBOX,
        )
        node_container.run()
        node_container.close()
        with pytest.raises(docker.errors.NotFound):
            docker_client.containers.get("blue")


def get_iproute2_image():
    return os.environ.get(
        "IMAGE_IPROUTE2", "registry.gitlab.com/abogdanenko/alpy/iproute2:latest"
    )


class TestNodeTap:
    @staticmethod
    def test_when_initialized_then_calling_close_is_ok(docker_client):
        node_tap = alpy.node.NodeTap(
            docker_client=docker_client,
            node_container_name="blue",
            interface_name="alpha",
            timeout=5,
            busybox_image=common.IMAGE_BUSYBOX,
            iproute2_image=get_iproute2_image(),
        )
        node_tap.close()

    @staticmethod
    def test_interface_is_created_in_host_namespace(docker_client):
        node_tap = alpy.node.NodeTap(
            docker_client=docker_client,
            node_container_name="blue",
            interface_name="alpha",
            timeout=5,
            busybox_image=common.IMAGE_BUSYBOX,
            iproute2_image=get_iproute2_image(),
        )
        node_tap.create_tap_interface()
        assert Path("/sys/class/net/alpha").exists()
        node_tap.close()

    @staticmethod
    def test_when_close_method_is_called_then_interface_is_removed(
        docker_client,
    ):
        node_tap = alpy.node.NodeTap(
            docker_client=docker_client,
            node_container_name="blue",
            interface_name="alpha",
            timeout=5,
            busybox_image=common.IMAGE_BUSYBOX,
            iproute2_image=get_iproute2_image(),
        )
        node_tap.create_tap_interface()
        node_tap.close()
        assert not Path("/sys/class/net/alpha").exists()

    @staticmethod
    @pytest.fixture
    def node_container(docker_client):
        container = docker_client.containers.create(
            common.IMAGE_BUSYBOX,
            ["cat"],
            auto_remove=True,
            network_mode="none",
            stdin_open=True,
        )
        container.start()
        time.sleep(3)
        container.reload()
        assert container.status == "running"
        yield container
        container.kill()

    @staticmethod
    def test_interface_disappears_from_host_namespace(
        docker_client, node_container
    ):
        node_tap = alpy.node.NodeTap(
            docker_client=docker_client,
            node_container_name=node_container.name,
            interface_name="alpha",
            timeout=5,
            busybox_image=common.IMAGE_BUSYBOX,
            iproute2_image=get_iproute2_image(),
        )
        node_tap.create_tap_interface()
        assert Path("/sys/class/net/alpha").exists()
        node_tap.setup_tap_interface()
        assert not Path("/sys/class/net/alpha").exists()
        node_tap.close()

    @staticmethod
    def is_interface_up(docker_client, container_name):
        # https://stackoverflow.com/questions/17679887/python-check-whether-a-network-interface-is-up/46932803#46932803
        output = docker_client.containers.run(
            common.IMAGE_BUSYBOX,
            ["cat", "/sys/class/net/eth0/flags"],
            network_mode="container:" + container_name,
            remove=True,
        )
        interface_flags = int(output, base=0)
        iff_up = 0x1
        return bool(interface_flags & iff_up)

    @staticmethod
    def test_interface_in_node_container_is_up(docker_client, node_container):
        node_tap = alpy.node.NodeTap(
            docker_client=docker_client,
            node_container_name=node_container.name,
            interface_name="alpha",
            timeout=5,
            busybox_image=common.IMAGE_BUSYBOX,
            iproute2_image=get_iproute2_image(),
        )
        node_tap.create_tap_interface()
        node_tap.setup_tap_interface()
        assert TestNodeTap.is_interface_up(docker_client, node_container.name)
        node_tap.close()


class TestSkeleton:
    @staticmethod
    def test_skeleton_with_no_nodes_is_ok():
        skeleton = alpy.node.Skeleton([])
        skeleton.create_tap_interfaces()
        skeleton.create()
        skeleton.close()

    @staticmethod
    def test_method_create_tap_interfaces(mocker):
        node = mocker.NonCallableMock(spec_set=alpy.node.Node)
        skeleton = alpy.node.Skeleton([node])
        skeleton.create_tap_interfaces()
        node.create_tap_interface.assert_called_once_with()
        skeleton.close()

    @staticmethod
    def test_method_create(mocker):
        node = mocker.NonCallableMock(spec_set=alpy.node.Node)
        skeleton = alpy.node.Skeleton([node])
        skeleton.create_tap_interfaces()
        node.reset_mock()
        skeleton.create()
        node.create.assert_called_once_with()
        skeleton.close()

    @staticmethod
    def test_method_close(mocker):
        node = mocker.NonCallableMock(spec_set=alpy.node.Node)
        skeleton = alpy.node.Skeleton([node])
        skeleton.create_tap_interfaces()
        skeleton.create()
        node.reset_mock()
        skeleton.close()
        node.close.assert_called_once_with()
