# SPDX-License-Identifier: GPL-3.0-or-later

import pexpect
import pytest

import alpy.remote_shell
import alpy.utils

PROMPT = "$ "


def set_prompt(console):
    console.sendline(f'PS1="{PROMPT}"')
    console.expect_exact("PS1=")
    console.expect_exact("\n")


class TestRandomDelimiter:
    # pylint: disable=protected-access
    @staticmethod
    def test_return_value_varies():
        assert (
            alpy.remote_shell._random_delimiter()
            != alpy.remote_shell._random_delimiter()
        )

    @staticmethod
    def test_return_value_length_is_constant():
        assert len(alpy.remote_shell._random_delimiter()) == len(
            alpy.remote_shell._random_delimiter()
        )

    @staticmethod
    def test_delimiter_is_short():
        assert len(alpy.remote_shell._random_delimiter()) <= 10


class TestUploadTextFile:
    @staticmethod
    def test_destination_file_has_the_same_contents(tmp_path):
        console = pexpect.spawn("sh")
        set_prompt(console)
        source_path = tmp_path / "source"
        destination_path = tmp_path / "destination"
        source_path.write_text("Hello, World!\n")
        alpy.remote_shell.upload_text_file(
            console, PROMPT, source_path, destination_path
        )
        console.expect_exact(PROMPT)
        console.sendeof()
        console.wait()
        assert destination_path.read_text() == "Hello, World!\n"


@pytest.fixture
def shell_console():
    console = pexpect.spawn("sh")
    set_prompt(console)
    yield console
    console.expect_exact(PROMPT)
    console.sendeof()
    console.wait()


class TestExecuteProgram:
    @staticmethod
    def test_when_program_exits_with_code_zero_then_function_returns_zero(
        shell_console,
    ):
        assert (
            alpy.remote_shell.execute_program(shell_console, PROMPT, "true", 5)
            == 0
        )

    @staticmethod
    def test_when_program_exits_with_code_one_then_function_returns_one(
        shell_console,
    ):
        assert (
            alpy.remote_shell.execute_program(shell_console, PROMPT, "false", 5)
            == 1
        )

    @staticmethod
    def test_when_timeout_then_exception_is_raised(shell_console):
        with pytest.raises(pexpect.TIMEOUT):
            alpy.remote_shell.execute_program(
                shell_console, PROMPT, "sleep 5", 4
            )


class TestCheckExecuteProgram:
    @staticmethod
    def test_when_program_exits_with_code_zero_then_exception_is_not_raised(
        shell_console,
    ):
        alpy.remote_shell.check_execute_program(
            shell_console, PROMPT, "true", 5
        )

    @staticmethod
    def test_program_exits_with_nonzero_code_then_exception_is_raised(
        shell_console,
    ):
        with pytest.raises(alpy.utils.NonZeroExitCode):
            alpy.remote_shell.check_execute_program(
                shell_console, PROMPT, "false", 5
            )


class TestUploadAndExecuteScript:
    @staticmethod
    def test_script_produced_side_effect_on_remote_host(tmp_path, monkeypatch):
        source_dir = tmp_path / "source"
        destination_dir = tmp_path / "destination"
        source_dir.mkdir()
        destination_dir.mkdir()
        console = pexpect.spawn("sh", cwd=destination_dir)
        set_prompt(console)
        (source_dir / "my-script").write_text("#!/bin/sh\ntouch side-effect\n")
        with monkeypatch.context() as patched_context:
            patched_context.chdir(source_dir)
            alpy.remote_shell.upload_and_execute_script(
                console, PROMPT, "my-script", 5
            )
        assert (destination_dir / "side-effect").exists()
        console.expect_exact(PROMPT)
        console.sendeof()
        console.wait()
