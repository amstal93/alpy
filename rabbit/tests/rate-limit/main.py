#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

"""This program is a test for network virtual appliance "rabbit".

The test checks that rabbit can rate-limit traffic.

.. code:: text

   +-------------------------------------+
   |                                     |
   |                rabbit               |
   |                                     |
   +-------+--------------------+--------+
           |                    |
           |                    |
           |                    |
   +-------+--------+   +-------+--------+
   |                |   |                |
   | 192.168.1.1/24 |   | 192.168.1.2/24 |
   |                |   |                |
   | node0          |   | node1          |
   |                |   |                |
   +----------------+   +----------------+

Steps
-----

1. Configure rabbit network interfaces.
2. Enable rate limiting on rabbit. Set limit to 1mbps.
3. Configure nodes.
4. Measure rate using iperf3 client on node 0 and iperf3 server on node 1.
5. If the rate measured is close to 1mbps, test passes.

Reference
---------

1. `How to Use the Linux Traffic Control
   <https://netbeez.net/blog/how-to-use-the-linux-traffic-control/>`_

2. `Traffic Shaping with tc
   <https://www.badunetworks.com/traffic-shaping-with-tc/>`_

3. `Network Performance Measurements with iperf
   <https://sandilands.info/sgordon/teaching/resources/iperf.html>`_

4. `Invoking iperf3 <https://software.es.net/iperf/invoking.html>`_
"""

import contextlib
import json
import logging
import math

import alpy.container
import alpy.utils

import carrot


def main():
    config = carrot.collect_config(link_count=2)

    alpy.utils.configure_logging()
    logger = logging.getLogger(__name__)
    logger.info("Test description: Check that rabbit rate-limits traffic.")

    with carrot.run(config) as resources:
        configure_nodes(resources.docker_client, config.timeout)
        with run_iperf3_server(resources.docker_client, config.timeout):
            with carrot.user_session(resources.console, resources.qmp):
                carrot.configure(resources.console, config.timeout)
                iperf3_report = run_iperf3_client(
                    resources.docker_client, config.timeout
                )
            check_rate(iperf3_report["end"]["sum_received"]["bits_per_second"])


def configure_nodes(docker_client, timeout):
    for node in [0, 1]:
        alpy.container.add_ip_address(
            f"node{node}",
            f"192.168.1.{node + 1}/24",
            docker_client=docker_client,
            image=carrot.IMAGE_BUSYBOX,
            timeout=timeout,
        )


@contextlib.contextmanager
def run_iperf3_server(docker_client, timeout):
    logger = logging.getLogger(__name__)
    context_logger = alpy.utils.make_context_logger(logger)
    container = None
    try:
        with context_logger("Start iperf3 server on node 1"):
            container = docker_client.containers.create(
                "mlabbe/iperf3:latest",
                ["--server", "--one-off"],
                healthcheck={"test": ["NONE"]},
                network_mode="container:node1",
            )
            container.start()
        try:
            yield
        finally:
            alpy.container.close(container, timeout)
    finally:
        if container:
            container.remove()


def run_iperf3_client(docker_client, timeout):
    logger = logging.getLogger(__name__)
    context_logger = alpy.utils.make_context_logger(logger)
    container = None
    try:
        with context_logger("Start iperf3 client on node 0"):
            container = docker_client.containers.create(
                "mlabbe/iperf3:latest",
                ["--client", "192.168.1.2", "--json"],
                healthcheck={"test": ["NONE"]},
                network_mode="container:node0",
            )
            container.start()
            with context_logger("Measure rate"):
                alpy.container.close(container, 10 + timeout)
            with context_logger("Parse iperf3 report"):
                iperf3_report_json = container.logs()
                return json.loads(iperf3_report_json)
    finally:
        if container:
            container.remove()


def check_rate(rate_bits_per_second):
    logger = logging.getLogger(__name__)
    logger.info(f"Rate received, bits per second: {rate_bits_per_second:.0f}")

    context_logger = alpy.utils.make_context_logger(logger)
    with context_logger("Check rate"):
        if not math.isclose(rate_bits_per_second, 1000000, rel_tol=0.5):
            raise RuntimeError(
                "Measured rate is different from configured rate limit"
            )


if __name__ == "__main__":
    main()
