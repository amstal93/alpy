#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

"""This program is a test for network virtual appliance "rabbit".

The test checks IPv4 routing.

.. code:: text

   +-------------------------------------+
   |                                     |
   |                rabbit               |
   |                                     |
   | 192.168.0.1/24      192.168.1.1/24  |
   |                                     |
   +-------+--------------------+--------+
           |                    |
           |                    |
           |                    |
   +-------+--------+   +-------+--------+
   |                |   |                |
   | 192.168.0.2/24 |   | 192.168.1.2/24 |
   |                |   |                |
   | node0          |   | node1          |
   |                |   |                |
   +----------------+   +----------------+

Steps
-----

1. Configure rabbit network interfaces.
2. Configure nodes.
3. Ping node 1 from node 0.
4. If ping is successfull, test passes.
"""

import logging

import alpy.container
import alpy.utils

import carrot


def main():
    config = carrot.collect_config(link_count=2)

    alpy.utils.configure_logging()
    logger = logging.getLogger(__name__)
    logger.info(
        "Test description: Check that rabbit forwards IPv4 packets by sending "
        "pings through it."
    )

    with carrot.run(config) as resources:
        configure_nodes(resources.docker_client, config.timeout)
        with carrot.user_session(resources.console, resources.qmp):
            carrot.configure(resources.console, config.timeout)
            ping_node1(resources.docker_client, config.timeout)


def configure_nodes(docker_client, timeout):
    for node in [0, 1]:
        alpy.container.configure_interface(
            f"node{node}",
            f"192.168.{node}.2/24",
            f"192.168.{node}.1",
            docker_client=docker_client,
            image=carrot.IMAGE_BUSYBOX,
            timeout=timeout,
        )


def ping_node1(docker_client, timeout):
    logger = logging.getLogger(__name__)
    context_logger = alpy.utils.make_context_logger(logger)
    with context_logger("Ping node 1 from node 0"):
        container = docker_client.containers.create(
            carrot.IMAGE_BUSYBOX,
            ["ping", "-c", "1", "192.168.1.2"],
            network_mode="container:node0",
        )
        try:
            container.start()
            alpy.container.close(container, timeout)
        finally:
            container.remove()


if __name__ == "__main__":
    main()
