#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import shutil
import subprocess
from pathlib import Path

LOGGER = logging.getLogger(Path(__file__).name)


def simple_call(command):
    args = command.split()
    LOGGER.debug(f"Call {args}")
    subprocess.run(args, check=True)


def set_hostname(hostname):
    LOGGER.info("Set hostname to " + hostname)
    Path("/etc/hostname").write_text(hostname + "\n", encoding="ascii")

    Path("/etc/hosts").write_text(
        "127.0.0.1 localhost\n"
        f"127.0.1.1 ${hostname}\n"
        "::1       localhost\n"
        f"::1       ${hostname}\n",
        encoding="ascii",
    )


def format_hdd():

    LOGGER.info("Install utilities")
    simple_call("apk add --no-progress parted eudev")

    LOGGER.info("Format the hard drive")

    simple_call(
        "parted --script --align optimal /dev/vda"
        " mktable gpt"
        " mkpart efi 1MiB 100%"
        " set 1 esp on"
        " set 1 boot on"
    )

    simple_call("udevadm settle")
    simple_call("mkfs.vfat -n ALPINE /dev/vda1")
    simple_call("apk del --no-progress parted eudev")

    LOGGER.info("Remove utilities")


def copy_files_from_cdrom():

    LOGGER.info("Copy files from CDROM")
    destination = Path("/media/vda1")
    destination.mkdir()
    simple_call(f"mount -t vfat /dev/vda1 {destination}")
    for path in Path("/media/cdrom").iterdir():
        if path.is_dir():
            shutil.copytree(path, destination / path.name)
        else:
            shutil.copy(path, destination)


def main():

    logging.basicConfig(
        level=logging.DEBUG,
        style="{",
        format="{levelname:8} {name:10} {message}",
    )

    set_hostname("rabbit")
    format_hdd()
    copy_files_from_cdrom()

    subprocess.run(
        ["sed", "-i", "s/cdrom/vda1/", "/etc/apk/repositories"], check=True
    )

    LOGGER.info("Write grub.cfg")
    Path("/media/vda1/boot/grub/grub.cfg").write_text(
        "linux /boot/vmlinuz-virt modules=loop,squashfs,sd-mod,usb-storage"
        " console=tty0 console=ttyS0,115200\n"
        "initrd /boot/initramfs-virt\n"
        "boot\n"
    )

    simple_call("setup-lbu vda1")
    simple_call("setup-apkcache /media/vda1/cache")
    simple_call("apk update")
    simple_call("apk cache download")
    simple_call("apk add --no-progress iproute2 nginx")
    simple_call("lbu commit")


if __name__ == "__main__":
    main()
