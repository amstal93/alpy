# SPDX-License-Identifier: GPL-3.0-or-later

import os

project = "Alpy API reference"  # pylint: disable=invalid-name
author = "Alexey Bogdanenko"  # pylint: disable=invalid-name

# Extensions
extensions = ["sphinx.ext.autodoc", "sphinx.ext.intersphinx"]

if "CI_PROJECT_URL" in os.environ:  # if executing in GitLab CI environment
    # link to code hosted on GitLab
    extensions.extend(["gitlab_link", "sphinx.ext.linkcode"])

autodoc_default_options = {"members": True, "undoc-members": True}
autodoc_mock_imports = ["pexpect", "qmp"]
intersphinx_mapping = {
    "docker": ("https://docker-py.readthedocs.io/en/stable", None),
    "pexpect": ("https://pexpect.readthedocs.io/en/stable", None),
    "python": ("https://docs.python.org/3", None),
}
